﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using bdUsers.bdEngine.Friends;

namespace bdUsers.bdEngine.Dialog
{
    public partial class DIalog : System.Windows.Forms.Form
    {
        public DIalog()
        {
            InitializeComponent();
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {            
            object item = listBoxChat.SelectedItem;
            int index = listBoxChat.SelectedIndex;

            if (index != -1)
            {
                if (DialogMas.chatList[Global.i][index].msgAccessU1 == true & DialogMas.chatList[Global.i][index].msgAccessU2 == true)
                {
                    textBoxDialog.ReadOnly = false;

                    if (item != null)
                    {
                        DialogMas.dialog[Global.i][index].Add(new DialogList(textBoxDialog.Text, ""));

                        string log2user = DialogMas.chatList[Global.i][index].login;
                        for (int i = 0; i < Global.users.Count; i++)
                        {
                            if (Global.users[i].login == log2user)
                            {
                                for (int j = 0; j < DialogMas.chatList[i].Count; j++)
                                {
                                    if (DialogMas.chatList[i][j].login == Global.users[Global.i].login)
                                    {
                                        DialogMas.dialog[i][j].Add(new DialogList("", textBoxDialog.Text));

                                    }
                                }
                            }
                        }

                        textBoxDialog.Clear();
                        RefreshDialog();
                    }

                    else
                    {
                        MessageBox.Show("Список пуст или не выбран пользователь.");
                        textBoxDialog.Clear();
                    }
                }
                else
                {
                    textBoxDialog.Clear();
                    textBoxDialog.Text = "Пользователь заблокирован";
                    textBoxDialog.ReadOnly = true;
                }
            }
                 
        }

        public void RefreshChatList()
        {
            listBoxChat.Items.Clear();

            for (int i = 0; i < DialogMas.chatList[Global.i].Count; i++)
            {
                listBoxChat.Items.Add(DialogMas.chatList[Global.i][i].name + " " + DialogMas.chatList[Global.i][i].surname);
            }
        }

        private void DIalog_Load(object sender, EventArgs e)
        {
            RefreshChatList();
        }

        public void RefreshDialog()
        {
            listBoxDialog.Items.Clear();

            for (int i = 0; i < DialogMas.dialog[Global.i][listBoxChat.SelectedIndex].Count; i++)
            {
                if (DialogMas.dialog[Global.i][listBoxChat.SelectedIndex][i].sendertUser != "")
                {
                    listBoxDialog.Items.Add(DialogMas.dialog[Global.i][listBoxChat.SelectedIndex][i].sendertUser);
                }
                else
                {
                    listBoxDialog.Items.Add("----"+DialogMas.dialog[Global.i][listBoxChat.SelectedIndex][i].gettingUser);
                }
            }
        }

        private void listBoxChat_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = listBoxChat.SelectedIndex;

            if (index != -1)
            {
                labelInfo.Text = $"Выбран пользователь: {DialogMas.chatList[Global.i][index].name} {DialogMas.chatList[Global.i][index].surname}";

                RefreshDialog();
            }
            else
            {
                MessageBox.Show("Выберите повторно");
            }
        }
    }
}
