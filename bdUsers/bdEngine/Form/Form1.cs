﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using bdUsers.bdEngine.PasswordCheck;
using bdUsers.bdEngine;
using System.IO;

namespace bdUsers
{
    public partial class Form : System.Windows.Forms.Form
    {
        public Form()
        {
            InitializeComponent();

            Refresher();
        }

        public void buttonCreate_Click(object sender, EventArgs e)
        {
            Anket anket = new Anket();
            anket.ShowDialog();

            Refresher();
        }

        private void buttonDeleteAll_Click(object sender, EventArgs e)
        {
            listBox.Items.Clear();

            for(int i=0;i<Global.users.Count;i++)
            {
                Global.users.Remove(Global.users[i]);
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            int id = listBox.SelectedIndex;

            object item = listBox.SelectedItem;

            if (item == null)
            {
                MessageBox.Show("Пользователь не выбран!!");
            }
            else
            {
                Global.users.Remove(Global.users[id]);
            }
            listBox.Items.Remove(item);
            listBox.SelectedItem = null;
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            if (Global.users.Count>0)
            {
                PassCheck pCheck = new PassCheck();
                pCheck.ShowDialog();

                Refresher();
            }
            else
            {
                MessageBox.Show("Список пуст");
            }
        }

        public void Refresher()
        {
            if (Global.users.Count > 0)
            {
                listBox.Items.Clear();

                for (int i = 0; i < Global.users.Count; i++)
                {
                    listBox.Items.Add(Global.users[i].login+" "+Global.users[i].name + "  " + Global.users[i].surname + "  " + Global.users[i].gender + " " + Global.users[i].city + " " + Global.users[i].dataBorn+" "+ Global.users[i].password);
                }
            }
        }

        private void buttonSaveIn_Click(object sender, EventArgs e)
        {
           
        }
    }
}
