﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using bdUsers.bdEngine.Friends;
using bdUsers.bdEngine.Update;
using bdUsers.bdEngine.Dialog;


namespace bdUsers.bdEngine.WelcomToProfile
{
    public partial class Profile : System.Windows.Forms.Form
    {
        public Profile()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UpdateProfil upd = new UpdateProfil();
            upd.ShowDialog();

            labelName.Text = $"{Global.users[Global.i].name} {Global.users[Global.i].surname}";

        }

        private void button2_Click(object sender, EventArgs e)
        {
            FriendsForm form = new FriendsForm();
            form.ShowDialog();            
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Global.users[Global.i].status = false;
            Close();
        }

        private void Profile_Load(object sender, EventArgs e)
        {
            labelName.Text =$"{Global.users[Global.i].name} {Global.users[Global.i].surname}";

            if (Global.users[Global.i].status==true)
            {
                labelStatus.Text = "Online";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DIalog dialog = new DIalog();
            dialog.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Coming soon!");
        }
    }
}
