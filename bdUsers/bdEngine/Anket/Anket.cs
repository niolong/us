﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using bdUsers.bdEngine;
using bdUsers.bdEngine.WelcomToProfile;
using bdUsers.bdEngine.Friends;
using bdUsers.bdEngine.Dialog;


namespace bdUsers
{
    public partial class Anket : System.Windows.Forms.Form
    {
        private Form form = new Form();
        private List<string> month;

        public Anket()
        {
            InitializeComponent();
        }

        private void Anket_Load(object sender, EventArgs e)
        {
            YearsHelper(comboBoxYears);
            MonthList(comboBoxMonth);
        }

        public void CheckGender(CheckBox box1, CheckBox box2)
        {
            if (box1.Checked == true)
            {
                box2.Enabled = false;
            }
            else if (box1.Checked == false)
            {
                box2.Enabled = true;
            }
        }

        private void checkBoxM_CheckedChanged(object sender, EventArgs e)
        {
            CheckGender(CheckBoxM, CheckBoxW);
        }

        private void CheckBoxW_CheckedChanged(object sender, EventArgs e)
        {
            CheckGender(CheckBoxW, CheckBoxM);
        }

        public string Gender(CheckBox m, CheckBox w)
        {
            string g = "";

            if (m.Checked != false & w.Checked == false || m.Checked == false & w.Checked != false)
            {
                if (m.Checked == true)
                {
                    g = "M";
                    return g;
                }
                else if (w.Checked == true)
                {
                    g = "Ж";
                    return g;
                }
            }
            else
            {
                MessageBox.Show("Пол не выбран!");
            }

            return g = "";
        }

        public string checkString(TextBox box)
        {
            string ans;
            do
            {
                if (box.Text != "")
                {
                    ans = box.Text;
                    return ans;
                }
                else
                {
                    MessageBox.Show("Поле не заполнено!");
                    return "";
                }
            } while (box.Text == "");
        }

        public int InputPassword(TextBox p)
        {
            if (p.Text != "")
            {
                int pass = 0;

                bool res = true;

                if (p.Text.Length < 4 || p.Text.Length > 10)
                {
                    MessageBox.Show("Пароль введен некоректно!");
                    p.Clear();
                    return 0;
                }

                res = int.TryParse(p.Text, out pass);

                if (res == false)
                {
                    MessageBox.Show("Пароль может содержать только числовые значения.");
                    return 0;
                }

                return pass;
            }
            else
            {
                MessageBox.Show("Поле пароля не заполнено!");
                return 0;
            }
        }

        public DateTime InputDataBorn(ComboBox d, ComboBox m, ComboBox y)
        {
            DateTime born;

            if (d.Text != "" & m.Text != "" & y.Text != "")
            {
                int M = 0;

                for (int i = 0; i < month.Count; i++)
                {
                    if (m.Text == month[i])
                    {
                        M = i;
                    }
                }
                string data = $"{d.Text}.{M+1}.{y.Text}";

                born = DateTime.Parse(data);

                return born;
            }
            else
            {
                MessageBox.Show("Дата рождения не заполнена!");

                string fallsDate = "1.1.1500";
                DateTime df = DateTime.Parse(fallsDate);

                return df;
            }
        }

        public void MonthList(ComboBox x)
        {
            month = new List<string>()
        {
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"
        };

            for (int i = 0; i < month.Count; i++)
            {
                x.Items.Add(month[i]);
            }

        }

        public void YearsHelper(ComboBox x)
        {
            int n, s;

            n = 2020 - 1950;
            s = 2020 - n;

            int[] yers = new int[n];

            for (int i = 0; i < yers.Length; i++)
            {
                x.Items.Add(yers[i] = s++);
            }
        }

        public string IputLogin(TextBox l)
        {
            string log = l.Text;

            if (l.Text != "")
            {
                if (l.Text.Length < 3 || l.Text.Length > 15 || l.Text == Global.adminLog)
                {
                    MessageBox.Show("Логин введен некоректно!");
                    textBoxLogin.Clear();
                    return "";
                }
                else
                {
                    bool exist = false;

                    for (int i = 0; i < Global.users.Count; i++)
                    {
                        if (l.Text == Global.users[i].login)
                        {
                            exist = true;
                            break;
                        }
                    }

                    if (exist == false)
                    {
                        return l.Text;
                    }
                    else
                    {
                        MessageBox.Show("Такой логин уже существует!");
                        return "";
                    }
                }
            }
            else
            {
                MessageBox.Show("Поле логина не заполнено!");
                return "";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string day = comboBoxDay.Text;
            string month = comboBoxMonth.Text;
            string years = comboBoxYears.Text;

            string login = IputLogin(textBoxLogin);
            string name = checkString(textBoxName);
            string surname = checkString(textBoxSurname);
            DateTime dataBorn = InputDataBorn(comboBoxDay, comboBoxMonth, comboBoxYears);
            string gender = Gender(CheckBoxM, CheckBoxW);
            string city = comboBoxCity.Text;
            int password = InputPassword(textBoxPassword);

            string fallsDate = "1.1.1500";
            DateTime df = DateTime.Parse(fallsDate);

            if (dataBorn != df & name != "" & surname != "" & gender != "" & password != 0 & login != "")
            {
                Global.users.Add(new User(login, name, surname, dataBorn, gender, city, password,false));
                Global.data.Add(new Data(day, month, years));
                FriendsMas.sendWait.Add(new List<FriendList>());
                FriendsMas.getWait.Add(new List<FriendList>());
                FriendsMas.usersFrendsMas.Add(new List<FriendList>());
                DialogMas.chatList.Add(new List<FriendList>());
                DialogMas.dialog.Add(new List<List<DialogList>>());

                if (Global.adminCheck == false)
                {
                    for (int i = 0; i < Global.users.Count; i++)
                    {
                        if(login==Global.users[i].login)
                        {
                            Global.i = i;
                        }
                    }
                    Global.users[Global.i].status = true;

                    Profile profile = new Profile();

                    profile.ShowDialog();
                    Close();
                }
                this.Close();
            }
        }
    }
}
