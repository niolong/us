﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bdUsers
{
    public class User
    {
        public string login;
        public string name;
        public string surname;
        public DateTime dataBorn;
        public string gender;
        public string city;
        public int password;
        public bool status;

        public User(string login,string name, string surname, DateTime dataBorn, string gender, string city, int password, bool status)
        {
            this.login = login;
            this.name = name;
            this.surname = surname;
            this.dataBorn = dataBorn;
            this.gender = gender;
            this.city = city;
            this.password = password;
            this.status = status;
        }


    }
}
