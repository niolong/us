﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bdUsers.bdEngine
{
    class Data
    {
        public string day;
        public string month;
        public string years;

        public Data(string day, string month,string years)
        {
            this.day = day;
            this.month = month;
            this.years = years;
        }
    }
}
