﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using bdUsers.bdEngine;
using bdUsers.bdEngine.PasswordCheck;

namespace bdUsers.bdEngine.EntryForm
{
    public partial class Entry : System.Windows.Forms.Form
    {
        public Form formHelper = new Form();

        public Entry()
        {
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            Global.adminCheck = false;
            Anket anket = new Anket();
            anket.ShowDialog();
        }

        private void buttonEnter_Click(object sender, EventArgs e)
        {
            Global.adminCheck = false;
            PassCheck checkAc = new PassCheck();
            checkAc.ShowDialog();
            
        }

    }
}
