﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using bdUsers.bdEngine.PasswordCheck;

namespace bdUsers.bdEngine.Update
{
    public partial class UpdateProfil : System.Windows.Forms.Form
    {
        public Anket anketHelper = new Anket();
        public int I ;

        public UpdateProfil()
        {
            InitializeComponent();

            I = Global.i;

            textBoxName.Text = Global.users[I].name;
            textBoxSurname.Text = Global.users[I].surname;
            comboBoxCity.Text = Global.users[I].city;
            textBoxPass.Text = $"{IntConvertor()}";
            comboBoxDay.Text = Global.data[I].day;
            comboBoxMonth.Text = Global.data[I].month;
            comboBoxYears.Text = Global.data[I].years;
            GenderConvertor();

            anketHelper.MonthList(comboBoxMonth);
            anketHelper.YearsHelper(comboBoxYears);
        }

        private string IntConvertor()
        {
            int x = Global.users[I].password;
            string pass = $"{x}";
            return pass;
        }

        public void GenderConvertor()
        {
            if(Global.users[I].gender=="M")
            {
                CheckBoxM.Checked = true;
            }
            else
            {
                CheckBoxW.Checked = true;
            }
        }

        private void buttonComplite_Click(object sender, EventArgs e)
        {
            string day = comboBoxDay.Text;
            string month = comboBoxMonth.Text;
            string years = comboBoxYears.Text;

            string name =anketHelper.checkString(textBoxName);
            string surname = anketHelper.checkString(textBoxSurname);
            DateTime dataBorn =anketHelper.InputDataBorn(comboBoxDay, comboBoxMonth, comboBoxYears);
            string city = comboBoxCity.Text;
            int password = anketHelper.InputPassword(textBoxPass);
            string gender = anketHelper.Gender(CheckBoxM, CheckBoxW);

            string fallsDate = "1.1.1500";
            DateTime df = DateTime.Parse(fallsDate);

            if (dataBorn != df & name != "" & surname != "" & gender != "" & password != 0)
            {
                Global.users[I].name = name;
                Global.users[I].surname = surname;
                Global.users[I].dataBorn = dataBorn;
                Global.users[I].city = city;
                Global.users[I].password = password;
                Global.users[I].gender = gender;

                Global.data[I].day = day;
                Global.data[I].month = month;
                Global.data[I].years = years;

                this.Close();
            }
        }

        private void CheckBoxM_CheckedChanged(object sender, EventArgs e)
        {
            anketHelper.CheckGender(CheckBoxM, CheckBoxW);
        }

        private void CheckBoxW_CheckedChanged(object sender, EventArgs e)
        {
            anketHelper.CheckGender(CheckBoxW, CheckBoxM);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
