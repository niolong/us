﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bdUsers.bdEngine.Friends
{
    class FriendList
    {
        public string login;
        public string name;
        public string surname;
        public bool msgAccessU1;
        public bool msgAccessU2;

        public FriendList(string name, string surname,bool msgAccess,string login, bool msgAccessU2)
        {
            this.name = name;
            this.surname = surname;
            this.msgAccessU1 = msgAccess;
            this.msgAccessU2 = msgAccessU2;
            this.login = login;
        }
    }
}
