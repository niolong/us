﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using bdUsers.bdEngine.Dialog;

namespace bdUsers.bdEngine.Friends
{
    public partial class FriendsForm : System.Windows.Forms.Form
    {
        public FriendsForm()
        {
            InitializeComponent();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void AddInSendBox()
        {
            listBoxSend.Items.Clear();

            for (int i = 0; i < FriendsMas.sendWait[Global.i].Count; i++)
            {
                listBoxSend.Items.Add(FriendsMas.sendWait[Global.i][i].name + " " + FriendsMas.sendWait[Global.i][i].surname);
            }
        }

        public void AddInGetBox()
        {
            listBoxGet.Items.Clear();

            if (FriendsMas.getWait[Global.i].Count > 0)
            {
                for (int i = 0; i < FriendsMas.getWait[Global.i].Count; i++)
                {
                    listBoxGet.Items.Add(FriendsMas.getWait[Global.i][i].name + " " + FriendsMas.getWait[Global.i][i].surname);
                }
            }
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            if (textBoxSearch.Text != "")
            {
                bool res = false;

                for (int i = 0; i < Global.users.Count; i++)
                {
                    if (Global.users[i].login == textBoxSearch.Text & textBoxSearch.Text != Global.users[Global.i].login)
                    {
                        res = true;

                        FriendsMas.sendWait[Global.i].Add(new FriendList(Global.users[i].name, Global.users[i].surname, true, Global.users[i].login, true));
                        AddInSendBox();

                        FriendsMas.getWait[i].Add(new FriendList(Global.users[Global.i].name, Global.users[Global.i].surname, true, Global.users[Global.i].login,true));
                        AddInGetBox();

                        textBoxSearch.Clear();
                    }
                }

                if (res == false)
                {
                    MessageBox.Show("Такого пользователя нет или логин введен неверно!");
                    textBoxSearch.Clear();
                }
            }
            else
            {
                MessageBox.Show("Логин не введен.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            object item = listBoxFriends.SelectedItem;

            if (item != null)
            {
                listBoxFriends.Items.Remove(item);
                FriendsMas.usersFrendsMas[Global.i].Remove(FriendsMas.usersFrendsMas[Global.i][listBoxFriends.SelectedIndex]);
            }
            else
            {
                MessageBox.Show("Список пуст");
            }
        }

        private void FriendsForm_Load(object sender, EventArgs e)
        {
            AddInSendBox();
            AddInGetBox();
            Refresher();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            int index = listBoxGet.SelectedIndex;
            object item = listBoxGet.SelectedItem;

            if (item != null)
            {
                string login2users = FriendsMas.getWait[Global.i][index].login;         

                FriendsMas.usersFrendsMas[Global.i].Add(new FriendList(FriendsMas.getWait[Global.i][index].name, FriendsMas.getWait[Global.i][index].surname, true, FriendsMas.getWait[Global.i][index].login, true));
            
                FriendsMas.getWait[Global.i].Remove(FriendsMas.getWait[Global.i][index]);
                AddInGetBox();
                Refresher();

                for (int i = 0; i < Global.users.Count; i++)
                {
                    if (Global.users[i].login == login2users)
                    {
                        FriendsMas.usersFrendsMas[i].Add(new FriendList(Global.users[Global.i].name, Global.users[Global.i].surname, true, Global.users[Global.i].login, true));
               
                        for (int j = 0; j < FriendsMas.sendWait[i].Count; j++)
                        {
                            if (FriendsMas.sendWait[i][j].login == Global.users[Global.i].login)
                            {
                                FriendsMas.sendWait[i].Remove(FriendsMas.sendWait[i][j]);
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Список пуст");
            }
        }

        public void Refresher()
        {
            listBoxFriends.Items.Clear();
            for (int i = 0; i < FriendsMas.usersFrendsMas[Global.i].Count; i++)
            {
                listBoxFriends.Items.Add(FriendsMas.usersFrendsMas[Global.i][i].name + " " + FriendsMas.usersFrendsMas[Global.i][i].surname);
            }
        }

        private void buttonCancellation_Click(object sender, EventArgs e)
        {
            object item = listBoxSend.SelectedItem;
            int index = listBoxSend.SelectedIndex;

            if (item != null)
            {
                string login2users = FriendsMas.sendWait[Global.i][index].login;

                FriendsMas.sendWait[Global.i].Remove(FriendsMas.sendWait[Global.i][index]);
                AddInSendBox();

                for (int i = 0; i < Global.users.Count; i++)
                {
                    if (Global.users[i].login == login2users)
                    {
                        for (int j = 0; j < FriendsMas.getWait[i].Count; j++)
                        {
                            if (FriendsMas.getWait[i][j].login == Global.users[Global.i].login)
                            {
                                FriendsMas.getWait[i].Remove(FriendsMas.getWait[i][j]);
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Отправленных запросов нет!");
            }
        }

        private static string CheckChatList(List<List<FriendList>> listChat, string log)
        {
            bool res = false;

            for (int i = 0; i < listChat[Global.i].Count; i++)
            {
                if (listChat[Global.i][i].login == log)
                {
                    res = true;
                }
            }

            if (res == false)
            {
                return log;
            }
            else
            {
                return "";
            }
        }

        private void ShowDialogChat()
        {
            DIalog dialog = new DIalog();
            dialog.ShowDialog();
            Close();
        }

        private void AddInChatList(int index, List<List<FriendList>> GetOrSend)
        {
            string log2user = GetOrSend[Global.i][index].login;

            for (int i = 0; i < Global.users.Count; i++)
            {
                if (log2user == Global.users[i].login)
                {
                    DialogMas.chatList[i].Add(new FriendList(Global.users[Global.i].name, Global.users[Global.i].surname, true, Global.users[Global.i].login, true));
                    DialogMas.dialog[i].Add(new List<DialogList>());
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            object itemGet = listBoxGet.SelectedItem;
            object itemSend = listBoxSend.SelectedItem;
            object itemFriend = listBoxFriends.SelectedItem;
            int indexGet = listBoxGet.SelectedIndex;
            int indexSend = listBoxSend.SelectedIndex;
            int indexFriend = listBoxFriends.SelectedIndex;

            if (itemGet != null)
            {
                string logCheckGet = FriendsMas.getWait[Global.i][indexGet].login;
                logCheckGet = CheckChatList(DialogMas.chatList, logCheckGet);

                if (logCheckGet != "")
                {
                    DialogMas.chatList[Global.i].Add(new FriendList(FriendsMas.getWait[Global.i][indexGet].name, FriendsMas.getWait[Global.i][indexGet].surname, true, FriendsMas.getWait[Global.i][indexGet].login, true));
                    DialogMas.dialog[Global.i].Add(new List<DialogList>());
                    AddInChatList(indexGet, FriendsMas.getWait);
                    ShowDialogChat();
                }
                else
                {
                    ShowDialogChat();
                }
            }
            else if (itemSend != null)
            {
                string logCheckSend = FriendsMas.sendWait[Global.i][indexSend].login;
                logCheckSend = CheckChatList(DialogMas.chatList, logCheckSend);

                if (logCheckSend != "")
                {
                    DialogMas.chatList[Global.i].Add(new FriendList(FriendsMas.sendWait[Global.i][indexSend].name, FriendsMas.sendWait[Global.i][indexSend].surname, true, FriendsMas.sendWait[Global.i][indexSend].login, true));
                    DialogMas.dialog[Global.i].Add(new List<DialogList>());
                    AddInChatList(indexSend, FriendsMas.sendWait);
                    ShowDialogChat();
                }
                else
                {
                    ShowDialogChat();
                }
            }
            else if (itemFriend != null)
            {
                string logCheckFriend = FriendsMas.usersFrendsMas[Global.i][indexFriend].login;
                logCheckFriend = CheckChatList(DialogMas.chatList, logCheckFriend);

                if (logCheckFriend != "")
                {
                    DialogMas.chatList[Global.i].Add(new FriendList(FriendsMas.usersFrendsMas[Global.i][indexFriend].name, FriendsMas.usersFrendsMas[Global.i][indexFriend].surname, true, FriendsMas.usersFrendsMas[Global.i][indexFriend].login, true));
                    DialogMas.dialog[Global.i].Add(new List<DialogList>());
                    AddInChatList(indexFriend, FriendsMas.usersFrendsMas);
                    ShowDialogChat();
                }
                else
                {
                    ShowDialogChat();
                }
            }
            else
            {
                MessageBox.Show("Пользователь не выбран!");
            }
        }

        public void BlockStatus(string log)
        {
            if (DialogMas.chatList[Global.i].Count > 0)
            {
                for (int k = 0; k < DialogMas.chatList[Global.i].Count; k++)
                {
                    if (DialogMas.chatList[Global.i][k].login == log)
                    {
                        for (int c = 0; c < Global.users.Count; c++)
                        {
                            if (log == Global.users[c].login)
                            {
                                for (int j = 0; j < DialogMas.chatList[c].Count; j++)
                                {
                                    if (Global.users[Global.i].login == DialogMas.chatList[c][j].login)
                                    {
                                        if (DialogMas.chatList[c][j].msgAccessU1 != false)
                                        {
                                            for (int i = 0; i < DialogMas.chatList[Global.i].Count; i++)
                                            {
                                                if (log == DialogMas.chatList[Global.i][i].login)
                                                {
                                                    if (DialogMas.chatList[Global.i][i].msgAccessU1 == true)
                                                    {
                                                        DialogMas.chatList[Global.i][i].msgAccessU1 = false;
                                                        MessageBox.Show("Пользователь заблокирован!");

                                                        for (int z = 0; z < Global.users.Count; z++)
                                                        {
                                                            if (Global.users[z].login == log)
                                                            {
                                                                for (int x = 0; x < DialogMas.chatList[z].Count; x++)
                                                                {
                                                                    if (DialogMas.chatList[z][x].login == Global.users[Global.i].login)
                                                                    {
                                                                        DialogMas.chatList[z][x].msgAccessU2 = false;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        DialogMas.chatList[Global.i][i].msgAccessU1 = true;
                                                        MessageBox.Show("Пользователь разблокирован!");

                                                        for(int h=0;h<Global.users.Count;h++)
                                                        {
                                                            if(Global.users[h].login==log)
                                                            {
                                                                for(int s=0;s<DialogMas.chatList[h].Count;s++)
                                                                {
                                                                    if(DialogMas.chatList[h][s].login==Global.users[Global.i].login)
                                                                    {
                                                                        DialogMas.chatList[h][s].msgAccessU2 = true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Вы заблокированы!");
                                           
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Чат с данным пользователем еще не создан!");
                    }
                }
            }
            else
            {
                MessageBox.Show("Чатов нет!");
            }
        }

        private void buttonBlock_Click(object sender, EventArgs e)
        {
            object itemGet = listBoxGet.SelectedItem;
            object itemSend = listBoxSend.SelectedItem;
            object itemFriend = listBoxFriends.SelectedItem;
            int indexGet = listBoxGet.SelectedIndex;
            int indexSend = listBoxSend.SelectedIndex;
            int indexFriend = listBoxFriends.SelectedIndex;

            if (itemGet != null)
            {
                string logGet = FriendsMas.getWait[Global.i][indexGet].login;

                BlockStatus(logGet);
            }
            else if (itemSend != null)
            {
                string logSend = FriendsMas.sendWait[Global.i][indexSend].login;
                BlockStatus(logSend);
            }
            else if (itemFriend != null)
            {
                string logFriend = FriendsMas.usersFrendsMas[Global.i][indexFriend].login;
                BlockStatus(logFriend);
            }
            else
            {
                MessageBox.Show("Пользователь не выбран!");
            }
        }
    }
}

