﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using bdUsers.bdEngine;
using bdUsers.bdEngine.Update;
using bdUsers.bdEngine.EntryForm;
using bdUsers.bdEngine.WelcomToProfile;

namespace bdUsers.bdEngine.PasswordCheck
{
    public partial class PassCheck : System.Windows.Forms.Form
    {
        public string boxPass,loginBox;

        public PassCheck()
        {
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public int Check()
        {
            if (textBoxCheckPass.Text != Global.adminPass & textBoxLoginCheck.Text != Global.adminLog)
            {
                for (int i = 0; i < Global.users.Count; i++)
                {
                    string pass = $"{Global.users[i].password}";
                    string log = Global.users[i].login;

                    if (pass == boxPass & log == loginBox)
                    {
                        textBoxCheckPass.Clear();
                        textBoxLoginCheck.Clear();
                        return i;
                    }
                }

                MessageBox.Show("Пароль или логин введен неверно!");
                textBoxCheckPass.Clear();
                textBoxLoginCheck.Clear();
            }
            return -1;
        }

        private void buttonAcceptPass_Click(object sender, EventArgs e)
        {
            Form form = new Form();
            Entry entry = new Entry();

            boxPass = textBoxCheckPass.Text;
            loginBox = textBoxLoginCheck.Text;

            int i = Check();

            if(textBoxLoginCheck.Text == Global.adminLog & textBoxCheckPass.Text==Global.adminPass)
            {
                Global.adminCheck = true;

                textBoxCheckPass.Clear();
                textBoxLoginCheck.Clear();

                form.ShowDialog();

                Close();
            }
            if (i!=-1 & Global.adminCheck==false)
            {
                entry.Close();
                this.Close();

                Global.i = i;
                Global.users[Global.i].status = true;

                Profile profile = new Profile();
                profile.ShowDialog();            
            }
            else  if(i != -1)
            {
                Global.i = i;
                UpdateProfil upd = new UpdateProfil();

                upd.ShowDialog();

                this.Close();
            }
        }
    }
}
