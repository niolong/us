﻿namespace bdUsers.bdEngine.PasswordCheck
{
    partial class PassCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PassCheck));
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonAcceptPass = new System.Windows.Forms.Button();
            this.textBoxCheckPass = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxLoginCheck = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.ForeColor = System.Drawing.Color.Black;
            this.buttonCancel.Location = new System.Drawing.Point(236, 191);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(92, 23);
            this.buttonCancel.TabIndex = 7;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonAcceptPass
            // 
            this.buttonAcceptPass.ForeColor = System.Drawing.Color.Black;
            this.buttonAcceptPass.Location = new System.Drawing.Point(179, 151);
            this.buttonAcceptPass.Name = "buttonAcceptPass";
            this.buttonAcceptPass.Size = new System.Drawing.Size(203, 23);
            this.buttonAcceptPass.TabIndex = 6;
            this.buttonAcceptPass.Text = "Принять";
            this.buttonAcceptPass.UseVisualStyleBackColor = true;
            this.buttonAcceptPass.Click += new System.EventHandler(this.buttonAcceptPass_Click);
            // 
            // textBoxCheckPass
            // 
            this.textBoxCheckPass.Location = new System.Drawing.Point(311, 102);
            this.textBoxCheckPass.Name = "textBoxCheckPass";
            this.textBoxCheckPass.Size = new System.Drawing.Size(201, 22);
            this.textBoxCheckPass.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(32, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(484, 29);
            this.label1.TabIndex = 4;
            this.label1.Text = "Введите данные для доступа к профилю";
            // 
            // textBoxLoginCheck
            // 
            this.textBoxLoginCheck.Location = new System.Drawing.Point(46, 102);
            this.textBoxLoginCheck.Name = "textBoxLoginCheck";
            this.textBoxLoginCheck.Size = new System.Drawing.Size(201, 22);
            this.textBoxLoginCheck.TabIndex = 8;
         
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(42, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Логин";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(307, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "Пароль";
            // 
            // PassCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(553, 226);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxLoginCheck);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonAcceptPass);
            this.Controls.Add(this.textBoxCheckPass);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.Green;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PassCheck";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Вход в профиль";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonAcceptPass;
        private System.Windows.Forms.TextBox textBoxCheckPass;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxLoginCheck;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}